function getEle(id) {
  return document.getElementById(id);
}
// test lay value   tip = tiền boa
function calculateTip() {
  var tongBill = getEle("billamt").value;
  //   console.log(tongBill);
  var haiLongVaTip = getEle("serviceQual").value;
  var soNguoiShare = getEle("peopleamt").value;
  // Check Validation
  if (tongBill === "" || haiLongVaTip == 0) {
    alert("Chon lai nhe");
    return;
  }
  // Kiểm tra có nhập vào số ng share
  if (soNguoiShare === "" || soNguoiShare <= 1) {
    soNguoiShare = 1;
    getEle("each").style.display = "none";
  } else {
    getEle("each").style.display = "block";
  }
  // Tinh toan
  var tongTip = (tongBill * haiLongVaTip) / soNguoiShare;
  // Làm tròn đến 2 chữ số tp
  tongTip = Math.round(tongTip * 100) / 100;
  // Hiển thị đúng 2 chũ số tp
  tongTip = tongTip.toFixed(2);
  // Hiển thị ra
  getEle("tip").innerHTML = tongTip;
  getEle("totalTip").style.display = "block";
  getEle("tip").style.display = "inline-block";
}
// Hiển thị vùng tiền tip
getEle("totalTip").style.display = "none";
getEle("tip").style.display = "none";

getEle("calculate").onclick = function () {
  calculateTip();
};
